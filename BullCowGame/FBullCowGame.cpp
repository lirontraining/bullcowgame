#include "FBullCowGame.h"
#include <map>
#define TMap std::map
#include <iostream>  //cout,endl,String
#include <ctime>

FBullCowGame::FBullCowGame(){
	Reset();
}//default constructor

void FBullCowGame::Reset()
{
	const FString HIDDEN_WORD = "liron";
	MyHiddenWord = HIDDEN_WORD;

	bIsGameWon = false;

	MyCurrentTry = 1;
	return;
}

int32 FBullCowGame::GetMaxTries() const {
	//calculate turns by using the length of the word
	int32 length = GetHiddenWordLength();
	length *= 1.3;
	return length + 1;
}

int32 FBullCowGame::getCurrentTry() const {return MyCurrentTry;}

int32 FBullCowGame::GetHiddenWordLength() const {return MyHiddenWord.length();}

bool FBullCowGame::isGameWon() const{

	return bIsGameWon;
}

EGuessStatus FBullCowGame::checkGuessValidity(FString G) const
{
	if (!IsLowerCase(G)) //if the guess is an isogram return error TODO write function
	{
		return EGuessStatus::NOT_LOWERCASE;
	}
	else if (!IsIsogram(G)) //if the guess isn't all lowercase return an error
	{
		return EGuessStatus::NOT_ISOGRAM;
	}
	else if (G.length() != GetHiddenWordLength()) //if the guess length is wrong return error TODO write function
	{
		return EGuessStatus::WRONG_LENGTH;
	}
	else //otherwise return OK
	{
		return EGuessStatus::OK;
	}
}

//recieve a VALID guess, incriments try counts and return count
FBullCowCount FBullCowGame::SubmitValidGuess(FString Guess)
{
	//increament the turn number
	MyCurrentTry++;

	//setup return variable
	FBullCowCount BullCowCount;

	//assuming same length as guess
	int32 HiddenWordLength = GetHiddenWordLength();

	//loop through all letters in the hidden word
	for (int32 MHWCar = 0; MHWCar < HiddenWordLength; MHWCar++)
	{
		//loop through all letters in the guess
		for (int32 GCar = 0; GCar < HiddenWordLength; GCar++)
		{
			//compare letters against the hidden word
			if (MyHiddenWord[MHWCar] == Guess[GCar]) 
			{
				if (MHWCar == GCar) 
				{
					BullCowCount.Bulls++;
				}
				else 
				{
					BullCowCount.Cows++;
				}
			}
		}
	}

	if (BullCowCount.Bulls == HiddenWordLength) {
		bIsGameWon = true;
	}

	return BullCowCount;
}

//check if a string is an isogram
bool FBullCowGame::IsIsogram(FString s) const
{

//treat 0 and 1 letter strings as isogram (true)
	if (s.length() <= 1) { return true; }
	
	//setup map
	TMap <char, bool> LetterSeen;

	for (auto L : s) //for all letters of the word
	{
		L = tolower(L);
		//letter already seen
		if (LetterSeen[L]) {
			return false;
		}
		//letter never seen
		LetterSeen[L] = true;
	}

	return true; //for example when /0 is entered
}

//check if a word is all in lowercase
bool FBullCowGame::IsLowerCase(FString s) const
{
	//loop the letters in the word
	for (auto l : s) {
		//check if the word is lower case
		if (!islower(l)) { return false; }
	}

	return true;
}

int32 FBullCowGame::getRandomNumber(int32 n) const
{
	int num = n;
	int digits = 0; while (num != 0) { num /= 10; digits++; }

	if (digits == 0) { return 0; }

	long int t = static_cast<long int> (time(NULL));
	//srand(t/n);
	int32 r = (rand() % 100);
	std::string val = std::to_string(((t / r)*n)+digits);
	val = val.substr(val.length() - digits, val.length());

	double random = atof(val.c_str()) / (digits*10);
	int32 returnValue = (int32)(random * (n + 1)) ;
	return returnValue;
}
