/*This is the console executable that makes use of the bullcow class
this acts as view in the MVC pattern, and is Responsible for all
user interaction. for game logic see the FBullCowGame
*/

#include <iostream>  //cout,endl,String
#include <string>    //cin
#include "FBullCowGame.h"
#include <ctime>

using FText = std::string;

void printIntro();
FText GetValidGuess();
void printGuess(FBullCowCount BullCowCount);
bool PlayGame(int32 turns);
bool askToPlayAgain();
void PrintGameSummery(bool didWin);

FBullCowGame BCGame; //instantiate

int main() {



	int32 MaxTries = BCGame.GetMaxTries();

	std::cout << MaxTries << std::endl;

	//constexpr int32 NUMBER_OF_TURNS = 2;
	bool bPlayAgain = false;
	do {
		printIntro();
		PlayGame(MaxTries);
		bPlayAgain = askToPlayAgain();
	} while (bPlayAgain);


	std::cout << std::endl;
	return 0;
}

//============= FUNCTIONS =============

//introduce the game to the user
void printIntro() 
{
	std::cout << "Welcome to Bulls and Cows !!\n";;
	std::cout << "Can you guess the " << BCGame.GetHiddenWordLength() << " letters isogram i'm thinking of?\n";
	std::cout << std::endl;
	return;
}


FText GetValidGuess() //get a VALID guess FText from the user
{
	int32 CurrentTry = BCGame.getCurrentTry();
	FText Guess= "";
	bool bIsValid = false;
	EGuessStatus Status = EGuessStatus::INVALID_STATUS;
	do 
	{
		std::cout << "Try."<< CurrentTry <<" Please enter your guess ("<< (BCGame.GetMaxTries()) <<" tries left): ";
		std::getline(std::cin, Guess);
	    EGuessStatus Status = BCGame.checkGuessValidity(Guess); 

		switch (Status)
		{
			case EGuessStatus::WRONG_LENGTH:
				std::cout << "Please enter a " << BCGame.GetHiddenWordLength() << " letter word.\n";
				break;
			case EGuessStatus::NOT_LOWERCASE:
				std::cout << "Please enter a word, all in lowercase.\n";
				break;
			case EGuessStatus::NOT_ISOGRAM:
				std::cout << "Please enter a word without repeating letters.\n";
				break;
			default:
				//assuming the guess is valid
				bIsValid = true;
				break;
        }
		
		if (!bIsValid) {
            std::cout << std::endl;
		}

	} while (!bIsValid); //keep looping untill we get no errors
	
	return	Guess;
}

//print a guess back to the user
void printGuess(FBullCowCount BullCowCount) 
{
	std::cout << "Bulls = " << BullCowCount.Bulls;
	std::cout << ", Cows = " << BullCowCount.Cows << "\n\n";
	return;
}

//start the game with the max number of turns available and return wether the player win or lose
bool PlayGame(int32 turns) 
{
	
	BCGame.Reset();
	int32 MaxTries = BCGame.GetMaxTries();

	//loop the number of turns for the game while is not won and there are still tries remaining
	while (!BCGame.isGameWon() && BCGame.getCurrentTry() <= MaxTries )
	{
		//get a valid guess from the user
		FText Guess = GetValidGuess();

		//TODO Submit valid guess to the game, and recieve counts
		FBullCowCount BullCowCount = BCGame.SubmitValidGuess(Guess);

		//prints the number of bulls and cows
		printGuess(BullCowCount);//TODO check for valid guess
	}

	//do something when the game is done
	PrintGameSummery(BCGame.isGameWon());

	return BCGame.isGameWon();
}

//check with the user if he want to play again
bool askToPlayAgain() 
{
	std::cout << "Do you want to play again? y/n" << std::endl;
	FText response = "";
	std::getline(std::cin, response);

	return (response[0] == 'y' || response[0] == 'Y');
}

//handle if the playr win or lose
void PrintGameSummery(bool didWin) 
{
	if (didWin) {
		std::cout << std::endl << "YAY YOU WON" << std::endl << std::endl;
	}
	else {
		std::cout << std::endl << "BOO YOU LOST" << std::endl << std::endl;
	}
	return;
}