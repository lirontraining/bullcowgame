#pragma once
#include <string>

using FString = std::string;
using int32 = int;

//like a class but all public (used for simple data structures)
// two integers, initialized to 0
struct FBullCowCount 
{
	int32 Bulls = 0;
	int32 Cows = 0;
};

enum class EGuessStatus 
{
	OK,
	NOT_ISOGRAM,
	WRONG_LENGTH,
	NOT_LOWERCASE,
	INVALID_STATUS
};

enum EResetStatus 
{
NO_HIDDEN_WORD
};

class FBullCowGame 
{
public:
	FBullCowGame(); // constructor
	int32 GetMaxTries() const;
	int32 getCurrentTry() const;
	int32 GetHiddenWordLength() const;

	bool isGameWon() const;
	EGuessStatus checkGuessValidity(FString) const;

	void Reset(); // TODO make it return some value
	//counts bulls and cows and increases the try number, assuming valid guess
	FBullCowCount SubmitValidGuess(FString);
	//TODO provide a method for countng bulls and cous and increasing try number
	//TODO function to increase try number

private:
	//initialized in the constructor
	int32 MyCurrentTry;
	bool bIsGameWon;
	int32 MagicNumber;
	FString MyHiddenWord;
	bool IsIsogram(FString) const;
	bool IsLowerCase(FString) const;
	int32 getRandomNumber(int32) const;
};